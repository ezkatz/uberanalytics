package com.uber.atc.server.handler;

import com.google.gson.JsonParser;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;
import com.uber.atc.server.model.AnalyticsRequest;
import com.uber.atc.server.model.Coordinate;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;


public class UberAnalyticsHandlerTest {

    @Test
    public void pickupsWithAllInclusiveBoundryAndAllInclusiveTimeRange() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(-100, 100));
        boundry.add(new Coordinate(100, 100));
        boundry.add(new Coordinate(100, -100));
        boundry.add(new Coordinate(-100, -100));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "01/01/2014 12:00 PM" , "01/01/2016 12:00 PM" );

        String pickups = handler.pickups(request);
        Assert.assertEquals("verify number of results", 1001, new JsonParser().parse(pickups).getAsJsonArray().size());
    }

    @Test
    public void pickupsWithAllInclusiveTimeRangeButSmallBoundry() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(40.7775,-73.9753));
        boundry.add(new Coordinate(40.7775, -73.9750));
        boundry.add(new Coordinate(40.7777, -73.9750));
        boundry.add(new Coordinate(40.7777, -73.9753));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "01/01/2014 12:00 PM" , "01/01/2016 12:00 PM" );

        String pickups = handler.pickups(request);
        Assert.assertEquals("verify number of results", 1, new JsonParser().parse(pickups).getAsJsonArray().size());
    }

    @Test
    public void pickupsWithAllInclusiveBoundryButSmallTimeRange() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(-100, 100));
        boundry.add(new Coordinate(100, 100));
        boundry.add(new Coordinate(100, -100));
        boundry.add(new Coordinate(-100, -100));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "04/01/2014 03:40 AM" , "04/01/2014 03:42 AM");

        String pickups = handler.pickups(request);
        Assert.assertEquals("verify number of results", 1, new JsonParser().parse(pickups).getAsJsonArray().size());
    }


    @Test
    public void dropoffsWithAllInclusiveBoundryAndAllInclusiveTimeRange() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(-100, 100));
        boundry.add(new Coordinate(100, 100));
        boundry.add(new Coordinate(100, -100));
        boundry.add(new Coordinate(-100, -100));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "01/01/2014 12:00 PM" , "01/01/2016 12:00 PM" );

        String pickups = handler.dropoffs(request);
        Assert.assertEquals("verify number of results", 1001, new JsonParser().parse(pickups).getAsJsonArray().size());
    }

    @Test
    public void dropoffsWithAllInclusiveTimeRangeButSmallBoundry() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(40.6482,-73.7830));
        boundry.add(new Coordinate(40.6482, -73.7828));
        boundry.add(new Coordinate(40.6484, -73.7828));
        boundry.add(new Coordinate(40.6484, -73.7830));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "01/01/2014 12:00 PM" , "01/01/2016 12:00 PM" );

        String pickups = handler.dropoffs(request);
        Assert.assertEquals("verify number of results", 1, new JsonParser().parse(pickups).getAsJsonArray().size());
    }

    @Test
    public void dropoffsWithAllInclusiveBoundryButSmallTimeRange() throws Exception {
        URL dataUrl = getClass().getResource("/server/data/uber-trips-data-test.csv");

        UberAnalyticsHandler handler = new UberAnalyticsHandler(dataUrl.getPath());

        MutableList<Coordinate> boundry = FastList.newList();
        boundry.add(new Coordinate(-100, 100));
        boundry.add(new Coordinate(100, 100));
        boundry.add(new Coordinate(100, -100));
        boundry.add(new Coordinate(-100, -100));

        AnalyticsRequest request  = new AnalyticsRequest(boundry, "04/01/2014 03:40 AM" , "04/01/2014 03:42 AM");

        String pickups = handler.dropoffs(request);
        Assert.assertEquals("verify number of results", 1, new JsonParser().parse(pickups).getAsJsonArray().size());
    }


}