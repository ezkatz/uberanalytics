package com.uber.atc.server.bootstrap;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

/*
 * This is an integration test which calls out to github and generates the bootstrap file. This should be moved
 * into an integration test folder and a proper unit test should be created.
 */
public class UberTripsFileInitializerTest {

    @Test
    public void bootstrapDataFileGeneration() throws Exception {
        File file = File.createTempFile("uber-trips-data-test", "csv");
        file.deleteOnExit();

        UberTripsFileInitializer.main(new String[]{"-output", file.getAbsolutePath()});
        List<String> actual = IOUtils.readLines(new FileInputStream(file));

        Assert.assertEquals("did not get expected number of rows", 2267136, actual.size());
        Assert.assertTrue("did not find expected entry", actual.contains("\"4/1/2014 02:43:00\",\"40.758\",\"-73.9761\",\"40.7238\",\"-73.9821\""));
        Assert.assertTrue("did not find expected entry", actual.contains("\"6/24/2014 21:09:00\",\"40.7552\",\"-73.9712\",\"40.7185\",\"-73.9501\""));
    }

}