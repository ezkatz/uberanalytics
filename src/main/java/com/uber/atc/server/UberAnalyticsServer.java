package com.uber.atc.server;


import com.uber.atc.server.handler.UberAnalyticsHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class UberAnalyticsServer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static void main(String[] args) {
        UberAnalyticsServer server = new UberAnalyticsServer();
        server.init();
        server.run();
        server.shutdown();
    }

    private void init() {
        //nothing to init for now
    }

    private void run() {
        try {
            ResourceConfig rc = new ResourceConfig();
            rc.register(new UberAnalyticsHandler(getClass().getResource("/server/data/uber-trips-data.csv").getPath()));

            final int port = System.getenv("PORT") != null ? Integer.valueOf(System.getenv("PORT")) : 8080;
            final URI baseUri = UriBuilder.fromUri("http://0.0.0.0/api/").port(port).build();
            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(baseUri, rc);

            //add static handlers
            StaticHttpHandler staticHttpHandler = new StaticHttpHandler("src/main/resources/web/");
            server.getServerConfiguration().addHttpHandler(staticHttpHandler, "/");

            server.start();

            // turn off file caching for development mode
            server.getListener("grizzly").getFileCache().setEnabled(false);
            staticHttpHandler.setFileCacheEnabled(false);
            logger.info(server.getServerConfiguration().getHttpServerName());
        } catch (Exception e) {
            System.err.println("ERROR: unable to start server: " + e.getMessage());
            System.exit(2);
        }
    }

    private void shutdown() {
        try {
            Thread.currentThread().join();
        } catch (Exception e) {
            logger.error("There was an error while starting Grizzly HTTP server.", e);
        } finally {
            logger.info("Server stopping");
        }
    }
}
