package com.uber.atc.server.handler;

import au.com.bytecode.opencsv.CSVReader;
import com.google.gson.Gson;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;
import com.uber.atc.server.bootstrap.UberTrip;
import com.uber.atc.server.model.AnalyticsRequest;
import com.uber.atc.server.model.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;


@Path("/data")
public class UberAnalyticsHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final int LAT_LNG_SCALING_FACTOR = 10000;

    private final SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

    private MutableList<UberTrip> trips = FastList.newList();

    public UberAnalyticsHandler(String filePath) throws IOException, ParseException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
        CSVReader reader = new CSVReader(in);
        reader.readNext(); //skip header
        String[] entry;
        while ((entry = reader.readNext()) != null) {
            trips.add(new UberTrip(entry));
        }

        Collections.shuffle(trips);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/pickups")
    public String pickups(AnalyticsRequest request) throws ParseException {
        MutableList<Coordinate> results = FastList.newList();

        Polygon polygon = getPolygon(request);

        for (UberTrip trip : trips) {
            boolean isContainedInBoundry = isContainedInPickupBoundry(polygon, trip);
            boolean isContainedInTimeRange = isContainedInTimeRange(request, trip);

            if (isContainedInBoundry && isContainedInTimeRange) {
                results.add(new Coordinate(trip.getLat(), trip.getLon()));
            }

            if (results.size() > 1000) {
                break;
            }
        }

        logger.info("returned {} pickup coordinates ", results.size());
        return new Gson().toJson(results.toArray());
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/dropoffs")
    public String dropoffs(AnalyticsRequest request) throws ParseException {
        MutableList<Coordinate> results = FastList.newList();

        Polygon polygon = getPolygon(request);

        for (UberTrip trip : trips) {
            boolean isContainedInBoundry = isContainedInDropoffBoundry(polygon, trip);
            boolean isContainedInTimeRange = isContainedInTimeRange(request, trip);

            if (isContainedInBoundry && isContainedInTimeRange) {
                results.add(new Coordinate(trip.getDropoffLat(), trip.getDropoffLon()));
            }

            if (results.size() > 1000) {
                break;
            }
        }

        logger.info("returned {} dropoff coordinates ", results.size());
        return new Gson().toJson(results.toArray());
    }

    private boolean isContainedInTimeRange(AnalyticsRequest request, UberTrip trip) throws ParseException {
        return timeFormat.parse(request.getBeginDateTime()).before(trip.getDate()) && timeFormat.parse(request.getEndDateTime()).after(trip.getDate());
    }

    private boolean isContainedInPickupBoundry(Polygon polygon, UberTrip trip) {
        return polygon.contains(Double.valueOf(trip.getLat() * LAT_LNG_SCALING_FACTOR).intValue(), Double.valueOf(trip.getLon() * LAT_LNG_SCALING_FACTOR).intValue());
    }

    private boolean isContainedInDropoffBoundry(Polygon polygon, UberTrip trip) {
        return polygon.contains(Double.valueOf(trip.getDropoffLat() * LAT_LNG_SCALING_FACTOR).intValue(), Double.valueOf(trip.getDropoffLon() * LAT_LNG_SCALING_FACTOR).intValue());
    }

    private Polygon getPolygon(AnalyticsRequest request) {
        Polygon polygon = new Polygon();

        for (Coordinate boundryCoordinate : request.getBoundry()) {
            polygon.addPoint(Double.valueOf(boundryCoordinate.getLat() * LAT_LNG_SCALING_FACTOR).intValue(),
                    Double.valueOf(boundryCoordinate.getLng() * LAT_LNG_SCALING_FACTOR).intValue());
        }

        return polygon;
    }
}
