package com.uber.atc.server.model;


import java.util.Date;

public class TimelineEntry {
    private Date date;
    private Integer count;

    public TimelineEntry() {
        //mandatory empty constructor
    }

    public TimelineEntry(Date date, Integer count) {
        this.date = date;
        this.count = count;
    }


    public Date getDate() {
        return date;
    }

    public Integer getCount() {
        return count;
    }
}
