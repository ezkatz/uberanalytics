package com.uber.atc.server.model;

import java.util.List;

public class AnalyticsRequest {

    private List<Coordinate> boundry;
    private String beginDateTime;
    private String endDateTime;

    public AnalyticsRequest() {
        //mandatory empty constructor
    }

    public AnalyticsRequest(List<Coordinate> boundry, String beginDateTime, String endDateTime) {
        this.boundry = boundry;
        this.beginDateTime = beginDateTime;
        this.endDateTime = endDateTime;
    }

    public List<Coordinate> getBoundry() {
        return boundry;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }
}
