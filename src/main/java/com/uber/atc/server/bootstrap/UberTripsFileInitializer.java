package com.uber.atc.server.bootstrap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UberTripsFileInitializer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MutableList<URL> uberRawDataUrls = FastList.newList();
    private CSVWriter writer;
    private String outputFilePath;

    public static void main(String[] args) throws IOException, ParseException, org.apache.commons.cli.ParseException {
        UberTripsFileInitializer main = new UberTripsFileInitializer();
        main.init(args);
        main.run();
    }

    private void init(String[] args) throws IOException, org.apache.commons.cli.ParseException {
        Options options = new Options();

        Option output = new Option("output", "output", true, "output file");
        output.setRequired(true);
        options.addOption(output);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        outputFilePath = cmd.getOptionValue("output");

        //setup output file
        writer = new CSVWriter(new FileWriter(outputFilePath));
        writer.writeNext(new String[]{"Date/Time", "Lat", "Lon", "dropoffLat", "dropoffLong"});
    }

    private void run() throws IOException, ParseException {
        //files to load
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-apr14.csv"));
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-may14.csv"));
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-jun14.csv"));
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-jul14.csv"));
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-aug14.csv"));
        uberRawDataUrls.add(new URL("https://raw.githubusercontent.com/fivethirtyeight/uber-tlc-foil-response/master/uber-trip-data/uber-raw-data-sep14.csv"));

        //write to output file
        for (URL uberRawDataUrl : uberRawDataUrls) {
            logger.info("loading file: {}", uberRawDataUrl);
            BufferedReader in = new BufferedReader(new InputStreamReader(uberRawDataUrl.openStream()));
            CSVReader reader = new CSVReader(in);
            reader.readNext(); //skip header

            String[] entry;
            while ((entry = reader.readNext()) != null) {
                UberRawTrip tripStart = new UberRawTrip(entry);

                String[] tripEndEntry = reader.readNext();
                if (tripEndEntry == null) {
                    continue; //odd number of rows in file so don't write to output file
                }

                UberRawTrip tripEnd = new UberRawTrip(tripEndEntry);

                UberTrip trip = new UberTrip(tripStart.getDate(), tripStart.getLat(), tripStart.getLon(), tripEnd.getLat(), tripEnd.getLon());

                writer.writeNext(trip.toArray());
            }

            logger.info("finished loading file: {}", uberRawDataUrl);
            logger.info("loaded file {} of {}", uberRawDataUrls.indexOf(uberRawDataUrl) + 1, uberRawDataUrls.size());
        }
    }

    private class UberRawTrip {
        private Date date;
        private double lat;
        private double lon;

        private UberRawTrip(String[] values) throws ParseException {
            this.date = new SimpleDateFormat("M/d/yyyy HH:mm:ss").parse(values[0]);
            this.lat = Double.parseDouble(values[1]);
            this.lon = Double.parseDouble(values[2]);
        }

        private Date getDate() {
            return date;
        }

        private double getLat() {
            return lat;
        }

        private double getLon() {
            return lon;
        }

    }


}
