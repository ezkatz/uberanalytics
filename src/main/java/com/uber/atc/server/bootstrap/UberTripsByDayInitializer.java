package com.uber.atc.server.bootstrap;

import au.com.bytecode.opencsv.CSVReader;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.api.multimap.list.MutableListMultimap;
import com.gs.collections.impl.list.mutable.FastList;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UberTripsByDayInitializer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    private MutableList<UberTrip> trips = FastList.newList();
    private String inputFilePath;
    private String outputFilePath;

    public static void main(String[] args) throws IOException, ParseException, org.apache.commons.cli.ParseException {
        UberTripsByDayInitializer main = new UberTripsByDayInitializer();
        main.init(args);
        main.run();
    }

    private void init(String[] args) throws org.apache.commons.cli.ParseException {
        Options options = new Options();

        Option input = new Option("input", "input", true, "input file path");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("output", "output", true, "output file");
        output.setRequired(true);
        options.addOption(output);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        inputFilePath = cmd.getOptionValue("input");
        outputFilePath = cmd.getOptionValue("output");
    }

    private void run() throws IOException, ParseException {
        logger.info("running {}", getClass());

        BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(inputFilePath)));
        CSVReader reader = new CSVReader(in);
        reader.readNext(); //skip header
        String[] entry;
        while ((entry = reader.readNext()) != null) {
            trips.add(new UberTrip(entry));
        }

        File file = new File(outputFilePath);
        Writer writer = new BufferedWriter(new FileWriter(file));

        MutableListMultimap<Date, UberTrip> tripsByDay = trips.groupBy(this::convertToDateOnly);

        writer.append("["); //open array
        tripsByDay.forEachKey(date -> {
            String yyyy = new SimpleDateFormat("yyyy").format(date);
            String m = new SimpleDateFormat("M").format(date);
            String d = new SimpleDateFormat("dd").format(date);
            try {
                writer.write("[new Date(" + yyyy + "," + m + "," + d + "), " + tripsByDay.get(date).size() + "] ");
            } catch (IOException e) {
                logger.error("unable to write to file", e);
            }
        });

        writer.append("]"); //closing array
        writer.flush();
        writer.close();
        logger.info("completed writing data for {} dates", tripsByDay.keysView().size());
        logger.info("output file {}", file.getAbsoluteFile());
    }

    private Date convertToDateOnly(UberTrip uberTrip) {
        String dateOnlyFormat = dateFormat.format(uberTrip.getDate());
        try {
            return dateFormat.parse(dateOnlyFormat);
        } catch (ParseException e) {
            throw new RuntimeException("unable to parse date");
        }
    }

}
