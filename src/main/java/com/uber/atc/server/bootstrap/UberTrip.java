package com.uber.atc.server.bootstrap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UberTrip {
    private Date date;
    private double lat;
    private double lon;
    private double dropoffLat;
    private double dropoffLon;

    public UberTrip(Date date, double lat, double lon, double dropoffLat, double dropoffLon) {
        this.date = date;
        this.lat = lat;
        this.lon = lon;
        this.dropoffLat = dropoffLat;
        this.dropoffLon = dropoffLon;
    }

    public UberTrip(String[] values) throws ParseException {
        this.date = new SimpleDateFormat("M/d/yyyy HH:mm:ss").parse(values[0]);
        this.lat = Double.parseDouble(values[1]);
        this.lon = Double.parseDouble(values[2]);
        this.dropoffLat = Double.parseDouble(values[3]);
        this.dropoffLon = Double.parseDouble(values[4]);
    }

    public Date getDate() {
        return date;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getDropoffLat() {
        return dropoffLat;
    }

    public double getDropoffLon() {
        return dropoffLon;
    }

    public String[] toArray() {
        return new String[]{new SimpleDateFormat("M/d/yyyy HH:mm:ss").format(date), String.valueOf(lat), String.valueOf(lon), String.valueOf(dropoffLat), String.valueOf(dropoffLon)};
    }
}
