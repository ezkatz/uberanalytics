import React from 'react';

class UberAnalyticsProgressBar extends React.Component {

  constructor(props) {
    super(props);
   }

  render() {
    return (
        <div className="analytics-progress-bar progress" style={{display: "none"}}>
            <div className="indeterminate"></div>
        </div>
    );
  }

}

export default UberAnalyticsProgressBar;