import React from 'react';
import {Tabs, Tab} from 'react-materialize';

class UberAnalyticsDropdownFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: 'pickups'};
    }

    onValueChanged(event){
         var activeTitle = $("[href='#tab_" + event + "']").text();
         this.setState({value: activeTitle});
         this.props.onValueChanged(activeTitle);
    }

    render() {
        return (
             <div className="analytics-types z-depth-1">
                    <Tabs onChange={(event) => this.onValueChanged(event)}>
                          <Tab className="analytics-type-tab" active={true} title={'pickups'} />
                          <Tab className="analytics-type-tab" title={'dropoffs'} />
                    </Tabs>
             </div>
        );
    }
}

export default UberAnalyticsDropdownFilter;