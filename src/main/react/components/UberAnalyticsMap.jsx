import React from "react";
import { withGoogleMap, GoogleMap, HeatmapLayer } from "react-google-maps";
import DrawingManager from "react-google-maps/lib/drawing/DrawingManager";
import UberAnalyticsDropdownFilter from './UberAnalyticsDropdownFilter.jsx';
import UberAnalyticsDateTimeFilter from './UberAnalyticsDateTimeFilter.jsx';
import UberAnalyticsProgressBar from './UberAnalyticsProgressBar.jsx';
import UberLogo from './UberLogo.jsx';

class UberAnalyticsMap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {startDateTime: '01/01/2014 12:00 PM', endDateTime:'06/01/2015 12:00 PM', dataType: 'pickups', heatmapOptions: {dissipating: true, radius: 10, opacity: 0.5}};
        this.clearHeatMap = this.clearHeatMap.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
        this.getBounds = this.getBounds.bind(this);
    }

    componentDidMount(){
        var map = new google.maps.Map(document.getElementById('map'), {center: {lat: 40.779355, lng: -73.966484}, zoom: 12, zoomControl: true, mapTypeControl: false, scaleControl: false, streetViewControl: false, rotateControl: false, fullscreenControl: false, styles: [{"elementType": "geometry", "stylers": [{"color": "#1d2c4d"} ] }, {"elementType": "labels.text.fill", "stylers": [{"color": "#8ec3b9"} ] }, {"elementType": "labels.text.stroke", "stylers": [{"color": "#1a3646"} ] }, {"featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{"color": "#4b6878"} ] }, {"featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{"color": "#64779e"} ] }, {"featureType": "administrative.province", "elementType": "geometry.stroke", "stylers": [{"color": "#4b6878"} ] }, {"featureType": "landscape.man_made", "elementType": "geometry.stroke", "stylers": [{"color": "#334e87"} ] }, {"featureType": "landscape.natural", "elementType": "geometry", "stylers": [{"color": "#023e58"} ] }, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#283d6a"} ] }, {"featureType": "poi", "elementType": "labels.text.fill", "stylers": [{"color": "#6f9ba5"} ] }, {"featureType": "poi", "elementType": "labels.text.stroke", "stylers": [{"color": "#1d2c4d"} ] }, {"featureType": "poi.business", "stylers": [{"visibility": "off"} ] }, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"color": "#023e58"} ] }, {"featureType": "poi.park", "elementType": "labels.text", "stylers": [{"visibility": "off"} ] }, {"featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{"color": "#3C7680"} ] }, {"featureType": "road", "elementType": "geometry", "stylers": [{"color": "#304a7d"} ] }, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#98a5be"} ] }, {"featureType": "road", "elementType": "labels.text.stroke", "stylers": [{"color": "#1d2c4d"} ] }, {"featureType": "road.highway", "elementType": "geometry", "stylers": [{"color": "#2c6675"} ] }, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#255763"} ] }, {"featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [{"color": "#b0d5ce"} ] }, {"featureType": "road.highway", "elementType": "labels.text.stroke", "stylers": [{"color": "#023e58"} ] }, {"featureType": "transit", "elementType": "labels.text.fill", "stylers": [{"color": "#98a5be"} ] }, {"featureType": "transit", "elementType": "labels.text.stroke", "stylers": [{"color": "#1d2c4d"} ] }, {"featureType": "transit.line", "elementType": "geometry.fill", "stylers": [{"color": "#283d6a"} ] }, {"featureType": "transit.station", "elementType": "geometry", "stylers": [{"color": "#3a4762"} ] }, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#0e1626"} ] }, {"featureType": "water", "elementType": "labels.text.fill", "stylers": [{"color": "#4e6d70"} ] } ]});
        var heatMapLayer = new google.maps.visualization.HeatmapLayer(this.state.heatmapOptions);

        var drawingManager = new google.maps.drawing.DrawingManager({drawingControl: true, drawingControlOptions: {position: google.maps.ControlPosition.TOP_CENTER, drawingModes: ['polygon']}, polygonOptions: { strokeColor: '#f7f8f9', fillColor: '#f7f8f9', fillOpacity: 0.01 }});
        drawingManager.setMap(map);

        this.setState({map: map, heatMapLayer: heatMapLayer, drawingManager: drawingManager});

        google.maps.event.addListener(map, 'idle', () => this.onBoundsChanged(map));
        google.maps.event.addListener(drawingManager, 'polygoncomplete', (polygon) => this.onPolygonComplete(polygon));
    }

    componentDidUpdate(){
          this.clearHeatMap();
          this.onDataRequest();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.heatmapData !== nextState.heatmapData) {
          return false;
        }

        if (this.state.dataRequest !== nextState.dataRequest) {
          return false;
        }

        return true;
    }

    onBoundsChanged(map) {
        if(this.state.polygon != null){
            return;
        }

        var boundry = [{"lat": map.getBounds().getNorthEast().lat(), "lng": map.getBounds().getNorthEast().lng()},
                      {"lat": map.getBounds().getNorthEast().lat(), "lng": map.getBounds().getSouthWest().lng()},
                      {"lat": map.getBounds().getSouthWest().lat(), "lng": map.getBounds().getSouthWest().lng()},
                      {"lat": map.getBounds().getSouthWest().lat(), "lng": map.getBounds().getNorthEast().lng()}];
        this.setState({boundry: boundry});
    }

    onPolygonComplete(polygon){
          this.state.drawingManager.setMap(null);
          $(".analytics-clear-boundry").show();

          this.clearHeatMap();
          var boundry =  this.getBounds(polygon.getPath())
          this.setState({boundry: boundry, polygon: polygon})
    }

    clearBoundry(){
           $(".analytics-clear-boundry").hide();
           this.state.drawingManager.setMap(this.state.map);
           this.state.polygon.setMap(null);
           this.state.polygon = null;
           this.onBoundsChanged(this.state.map);
    }

    getBounds(path){
        var bounds = [];

        path.forEach(function(segment) {
            bounds.push({"lat": segment.lat(), "lng": segment.lng()});
        });

        return bounds;
    }

    setDateTimeRange(dateTimeFilterState){
      this.setState(dateTimeFilterState);
    }

    setDataType(dataType){
      this.setState({dataType: dataType});
    }

    clearHeatMap(){
      if(this.state.heatMapLayer != null){
        this.state.heatMapLayer.setMap(null);
      }
    }

    onSuccess(result){
         $(".analytics-progress-bar").hide();
         var heatmapData = [];
         for (var i = 0; i < result.length; i++) {
           var coords = result[i];
           var latLng = new google.maps.LatLng(coords.lat, coords.lng);
           heatmapData.push(latLng);
         }

         this.state.heatMapLayer = new google.maps.visualization.HeatmapLayer(this.state.heatmapOptions);
         this.state.heatMapLayer.setData(heatmapData);
         this.state.heatMapLayer.setMap(this.state.map);
     }

     onError(error, xhr){
         if (xhr.statusText === 'abort') {
            return;
         }

         $(".analytics-progress-bar").hide();
          console.log(error);
      }

   onDataRequest(){
      $(".analytics-progress-bar").show();

      var dataRequest = $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/api/data/" + this.state.dataType,
            data: JSON.stringify({ boundry: this.state.boundry, beginDateTime: this.state.startDateTime, endDateTime: this.state.endDateTime}),
            dataType: "json",
            success: this.onSuccess,
            error: this.onError
        });

        if(this.state.dataRequest != null){
           this.state.dataRequest.abort();
        }

        this.setState({'dataRequest': dataRequest});
    }

    render() {
        return (<div style={{width: "100%", height: "100%"}}>
                    <div id="map" />
                    <div className="analytics-clear-boundry" style={{display: "none"}} onClick={() => this.clearBoundry()}> Clear Boundry</div>
                    <UberAnalyticsDropdownFilter onValueChanged={(dataType) => this.setDataType(dataType)} />
                    <UberAnalyticsDateTimeFilter onValueChanged={(dateTimeFilterState) => this.setDateTimeRange(dateTimeFilterState)} startDateTime={this.state.startDateTime} endDateTime={this.state.endDateTime}/>
                    <UberAnalyticsProgressBar />
                    <UberLogo />
                </div>);
    }
}

export default UberAnalyticsMap;