import React from 'react';
import { render } from 'react-dom';
import UberLogo from './components/UberLogo.jsx';
import UberAnalyticsMap from './components/UberAnalyticsMap.jsx';

class App extends React.Component {
  render () {
    return (
        <div style={{width: "100%", height: "100%"}}>
            <UberAnalyticsMap />
         </div>
    );
  }
}

render(<App/>, document.getElementById('app'));
