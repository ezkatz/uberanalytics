var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/main/resources/web/assets/js/');
var APP_DIR = path.resolve(__dirname, 'src/main/react/');

module.exports = {
                   entry: APP_DIR + '/index.jsx',
                   output: {
                     path: BUILD_DIR,
                     filename: 'bundle.js'
                   },
                   resolve: {
                       extensions: ['', '.js', '.jsx', '.json']
                   },
                   module : {
                     loaders : [
                       {
                         test : /\.jsx?/,
                         include : APP_DIR,
                         loader : 'babel'
                       }
                     ]
                   }
                 };