UberAnalytics - a way to visualize Uber trip data
==================================
`UberAnalytics` is a web-app to view pickup and dropoff data with the ability to geo-fence
a region and define start and end time criteria.  The source data was retrieved from
[uber-trip-data](https://github.com/fivethirtyeight/uber-tlc-foil-response/tree/master/uber-trip-data).

Using UberAnalytics
==================
`UberAnalytics` is hosted on AWS and is accessible via the link below:
[http://uberanalyticsdemo.us-east-1.elasticbeanstalk.com/](http://uberanalyticsdemo.us-east-1.elasticbeanstalk.com/)

Using the app is pretty simple. It's a single page UI with a toggle to switch between pickup and dropoff mode on the
upper-right hand of the page. In order to change the time range, use the timeline on the lower-left hand side of the page.
Whenever data is refreshing a loading ribbon will appear on the bottom of the page.

On the top-center of the page, there is polygon widget which allows you to draw a shape and once the shape is connected the map will only show data within the
boundry. Click "Clear Boundry" on the top-center of the page to get the whole map view.

Building from source
====================
## Building client side bundle ##
* cd <project_root>
* npm install
* npm run build or npm run dev

## Building server side jar ##
* cd <project_root>
* mvn package

## Deploying ##
### Local ###
* Start "UberAnalyticsServer - Start" in the run configuration

### AWS ##
*To deploy the app in AWS, create a Java 8 instance and upload the jar from mvn package to your AWS instance. The embedded Grizzly Server will serve the web content and start up the REST server. `UberAnalytics` requires a t2.medium instance since it loads all the pickup/dropoff data into memory.

## Data Bootstrapping ##
*Start "Run Bootstrap - UberTripsFileInitializer" in the run configuration. This will generate the data file with all the time, pickup and dropoff points from [uber-trip-data](https://github.com/fivethirtyeight/uber-tlc-foil-response/tree/master/uber-trip-data).
*Start "Run Bootstrap - UberTripsByDayInitializer" in the run configuration This will output the number of trips per day which is used for the timeline widget.

Technology Stack
==================
* Java Backend using
*    Grizzly Webserver
*    Jersey for REST endpoints
*    GSON for serialization
*    Eclipse Collections (previously GS-Collections) for enhanced collection framework
* ReactJS Front-End using
*    Google Maps
*    Google Charts for the Timeline
*    Moment for date parsing
* Hosted On AWS

Design Considerations
==================
This project take the This project contains both the client and server side code bundled into a "fat" jar. I like this approach for small projects since it makes developing in the IDE and deploying simple.


Open Issues/Future Development
==================
1. Improve performance of getting data as the user drags and zooms over the map
2. Client-Side unit tests (currently, there are none due to time constrains)
3. Improve the way large configs are imported in the JSX code. For example, the Google Maps Config json is inlined in the
code but I would like to have it in a seperate file where its formatted and import it into the JSX. The same is true for the data
points for the Timeline widget.
4. Cleanup java and web dependencies that I was testing out and but didn't use in the project